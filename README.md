# Terraform and Istio

## Scope

This repo is designed to spin up 3 k8s clusters
on the IBM cloud in differest states to help
tell the story of <https://github.com/jjasghar/istio-overview>.

## PreReqs

You'll need the [ibmcloud cli](https://console.bluemix.net/docs/cli/index.html#overview) installed,
I suggest you use the `curl || bash` if you can. It brings in the plugins and all.

```bash
curl -sL https://ibm.biz/idt-installer | bash
```

You'll need istio install in this directory, the easiest way is to `git clone`
this repo down then run the following in the directory.

```bash
curl -L https://git.io/getLatestIstio | sh -
```

You'll want to set these `ENVIRONMENT` vars also:

```bash
TF_VAR_ibm_sl_username=YOUR_USERNAME@example.com
TF_VAR_ibm_sl_api_key=fffd0923aa667c617a62f51MY_FAKE_API_KEYd06cc9903543f1e85
TF_VAR_ibm_bx_api_key=hyvRrHFTo2ms6-MY_REALLY_FAKE_APIKEY-SrrOV
```

Edit the following in the `spinup.sh` script:

```bash
CLUSTER_PREFIX_NAME=jj
ISTIO_VERSION=1.0.5
```

You'll also want to install at least [inspec](https://www.inspec.io/) for the testing.

```bash
curl https://omnitruck.chef.io/install.sh | sudo bash -s -- -P inspec
```

## Usage

You have two scripts in the root of the repository, run either to do the magic.

- `spinup.sh` : spins up the environment
- `spindown.sh` : blows everything away

## Bonus

- `ibm_istio_webpage.sh` : helps open up the ingress gateway so you don't have to do that crazy incantation

```bash
ibm_istio_webpage.sh <PREFIX>-istio-overview-part-X # opens firefox on the productpage
```

## License & Authors

If you would like to see the detailed LICENCE click [here](./LICENCE).

- Author: JJ Asghar <jja@ibm.com>

```text
Copyright:: 2018- IBM, Inc

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
