provider "ibm" {
  bluemix_api_key    = "${var.ibm_bx_api_key}"
  softlayer_username = "${var.ibm_sl_username}"
  softlayer_api_key  = "${var.ibm_sl_api_key}"
}

variable ibm_bx_api_key {}
variable ibm_sl_username {}
variable ibm_sl_api_key {}
data "ibm_space" "spacedata" {
  space = "dev"   # this will be different if you aren't is this space
  org   = "Developer Advocacy" # this will be different if you aren't is this org
}

data "ibm_org" "org" {
  org = "${data.ibm_space.spacedata.org}"
}

data "ibm_space" "space" {
  org   = "${data.ibm_space.spacedata.org}"
  space = "${data.ibm_space.spacedata.space}"
}

data "ibm_account" "account" {
  org_guid = "${data.ibm_org.org.id}"
}

data "ibm_resource_group" "jjk8s-cluster-group" {
  name = "default"
}

resource "ibm_container_cluster" "jj-istio-overview-part-0" { # you should change this
  name            = "jj-istio-overview-part-0" # you should change this
  datacenter      = "dal13" # you should change this
  machine_type    = "u2c.2x4"
  hardware        = "shared"
  billing         = "hourly"
  public_vlan_id  = "2272296"
  private_vlan_id = "2272298"
  worker_num      = 1
  account_guid = "47b84451ab70b94737518f7640a9ee42" # you should change this
}

resource "ibm_container_cluster" "jj-istio-overview-part-1" { # you should change this
  name            = "jj-istio-overview-part-1" # you should change this
  datacenter      = "dal13" # you should change this
  machine_type    = "u2c.2x4"
  hardware        = "shared"
  billing         = "hourly"
  public_vlan_id  = "2272296"
  private_vlan_id = "2272298"
  worker_num      = 1
  account_guid = "47b84451ab70b94737518f7640a9ee42" # you should change this
}

 resource "ibm_container_cluster" "jj-istio-overview-part-2" { # you should change this
   name            = "jj-istio-overview-part-2" # you should change this
   datacenter      = "dal13"
   machine_type    = "u2c.2x4"
   hardware        = "shared"
   billing         = "hourly"
   public_vlan_id  = "2272296"
   private_vlan_id = "2272298"
   worker_num      = 1
   account_guid = "47b84451ab70b94737518f7640a9ee42" # you should change this
}
