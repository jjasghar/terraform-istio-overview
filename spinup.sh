#!/bin/bash

CLUSTER_PREFIX_NAME=jj
ISTIO_VERSION=1.0.5

install_base_part0() {
  eval "$(ibmcloud ks cluster-config --export $CLUSTER_PREFIX_NAME-istio-overview-part-0)"
  sleep 10
  helm init --service-account tiller
  sleep 10
  kubectl apply -f istio-$ISTIO_VERSION/install/kubernetes/helm/helm-service-account.yaml
}

validate_istio_part0() {
  inspec exec controls/istio-part0/01_istio_base.rb
}

install_istio_part1() {
  eval "$(ibmcloud ks cluster-config --export $CLUSTER_PREFIX_NAME-istio-overview-part-1)"
  sleep 10
  helm init --service-account tiller
  sleep 10
  kubectl apply -f istio-$ISTIO_VERSION/install/kubernetes/helm/helm-service-account.yaml
  sleep 10
  helm install istio-$ISTIO_VERSION/install/kubernetes/helm/istio --name istio --namespace istio-system
  kubectl create -f istio-$ISTIO_VERSION/install/kubernetes/istio-demo.yaml
}

validate_istio_part1() {
  inspec exec /home/$CLUSTER_PREFIX_NAME/cloud/k8s/istio-overview/controls/istio-part1/01_istio_base.rb
}

install_istio_part2() {
  eval "$(ibmcloud ks cluster-config --export $CLUSTER_PREFIX_NAME-istio-overview-part-2)"
  sleep 10
  helm init --service-account tiller
  sleep 10
  kubectl apply -f istio-$ISTIO_VERSION/install/kubernetes/helm/helm-service-account.yaml
  sleep 10
  helm install istio-$ISTIO_VERSION/install/kubernetes/helm/istio --name istio --namespace istio-system
  kubectl create -f istio-$ISTIO_VERSION/install/kubernetes/istio-demo.yaml
  kubectl apply -f <(istioctl kube-inject -f istio-$ISTIO_VERSION/samples/bookinfo/platform/kube/bookinfo.yaml)
  kubectl apply -f istio-$ISTIO_VERSION/samples/bookinfo/networking/bookinfo-gateway.yaml
  kubectl apply -f istio-$ISTIO_VERSION/samples/bookinfo/networking/destination-rule-all.yaml
}

validate_istio_part2() {
  inspec exec controls/istio-part2/01_istio_base.rb
}

hello_world_test() {
  eval "$(ibmcloud ks cluster-config "$1" | grep export)"
  echo ""
  echo "Spinning up hello-world..."
  kubectl run hello-world --replicas=2 --labels="app=testapp" --image=gcr.io/google-samples/node-hello:1.0  --port=8080  &>/dev/null
  kubectl expose deployment hello-world --type LoadBalancer --port 80 --target-port 8080 &>/dev/null
  echo "Sleeping for 30 seconds because of IPs and such..."
  sleep 30
  IP=$(kubectl get service hello-world | awk 'FNR==2{print $4}')
  echo "$1 IP is: $IP"
  if [ "$IP" = "<pending>" ]
  then
    echo "$1: FAIL FAIL FAIL FAIL FAIL FAIL FAIL FAIL FAIL FAIL FAIL FAIL FAIL FAIL NO IP "
    # exit 1
  else
    OUTPUT=$(curl --connect-timeout 5 --retry 5 "$IP")
    if [ "${OUTPUT}" = "Hello Kubernetes!" ]
    then
      echo ""
      echo "$1: PASS"
      echo ""
    else
      echo "$1: FAIL FAIL FAIL FAIL FAIL FAIL FAIL FAIL FAIL FAIL FAIL FAIL FAIL FAIL "
      # exit 1
    fi
  fi
  echo "Cleaned up!"
  kubectl delete deployment hello-world &>/dev/null
  kubectl delete service hello-world &>/dev/null
}

main() {
  # spin everything up.
  terraform apply -auto-approve
  install_base_part0
  install_istio_part1
  install_istio_part2
  hello_world_test $CLUSTER_PREFIX_NAME-istio-overview-part-0
  hello_world_test $CLUSTER_PREFIX_NAME-istio-overview-part-1
  hello_world_test $CLUSTER_PREFIX_NAME-istio-overview-part-2
  validate_istio_part0
  validate_istio_part1
  validate_istio_part2
}

main
