#!/bin/bash

if [ $# -eq 0 ]
then
  echo "You need to add a name for this cluster, ibm_istio_webpage.sh NAME"
  exit 1
else
  $(ibmcloud ks cluster-config --export "$1")
  export INGRESS_HOST=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
  export INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].port}')
  export SECURE_INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="https")].port}')
  export GATEWAY_URL=$INGRESS_HOST:$INGRESS_PORT
  is_the_site_200=`curl -o /dev/null -s -w "%{http_code}\n" http://${GATEWAY_URL}/productpage`
  if [ ${is_the_site_200} -ne "200" ]
     then
     echo "#"
     echo "# Site isn't up or giving something other then 200"
     echo "#"
  else
    firefox http://${GATEWAY_URL}/productpage
  fi
fi
