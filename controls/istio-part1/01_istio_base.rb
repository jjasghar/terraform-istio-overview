control 'helm_version_check' do
  impact 1.0
  title 'Check the helm version on the cluster'
  describe command('$(ibmcloud ks cluster-config --export jj-istio-overview-part-1) && helm version') do
    its('stdout') { should_not match /Error: could not find tiller/ }
    its('exit_status') { should eq 0 }
  end
end

control 'istio_namespace_check' do
  impact 1.0
  title 'Check the istio namespace exists on the cluster'
  describe command('$(ibmcloud ks cluster-config --export jj-istio-overview-part-1) && kubectl get namespaces') do
    its('stdout') { should match /istio-system/ }
    its('exit_status') { should eq 0 }
  end
end

control 'istio_demo_check' do
  impact 1.0
  title 'Check the istio demo sanity check'
  describe command('$(ibmcloud ks cluster-config --export jj-istio-overview-part-1) && kubectl get services --namespace istio-system') do
    its('stdout') { should match /servicegraph/ }
    its('stdout') { should match /zipkin/ }
    its('stdout') { should match /jaeger-query/ }
    its('stdout') { should match /jaeger-collector/ }
    its('stdout') { should match /jaeger-agent/ }
    its('exit_status') { should eq 0 }
  end
end
