control 'helm_version_check' do
  impact 1.0
  title 'Check the helm version on the cluster'
  describe command('$(ibmcloud ks cluster-config --export jj-istio-overview-part-1) && helm version') do
    its('stdout') { should_not match /Error: could not find tiller/ }
    its('exit_status') { should eq 0 }
  end
end
